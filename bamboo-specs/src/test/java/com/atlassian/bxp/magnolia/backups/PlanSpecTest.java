package com.atlassian.bxp.magnolia.backups;

import com.atlassian.bamboo.specs.api.util.EntityPropertiesBuilders;
import org.junit.Assert;
import org.junit.Test;

import java.time.LocalTime;
import java.util.Arrays;

/**
 * Junit tests for PlanSpec
 */
public class PlanSpecTest {
    @Test
    public void checkYourPlanOffline() {
        Arrays.asList(PlanSpec.Branch.values()).forEach(
                branch -> EntityPropertiesBuilders.build(PlanSpec.createPlan(branch)));
    }

    @Test
    public void checkCustomDeleteStable() {
        Assert.assertEquals(PlanSpec.getDeleteConfStable(), "true");
    }

    @Test
    public void testMySchedule() {
        // Schedule for UTC time at 8 AM is the same as LA at 0 AM. LA time is 8 hours behind UTC.
        // Bamboo server schedule base trigger uses the server's LocalTime with its system default time zone.

        Assert.assertEquals(new PlanSpec.MySchedule("UTC", 8, 0, 0).getTime(),
                new PlanSpec.MySchedule("America/Los_Angeles",0,0,0).getTime());
        Assert.assertEquals(new PlanSpec.MySchedule("UTC", 0, 0, 0).getTime(),
                new PlanSpec.MySchedule("America/Los_Angeles",16,0,0).getTime());

        // Schedule for Chicago time at 7 AM is the same as LA at 5 AM

        Assert.assertEquals(
                new PlanSpec.MySchedule("America/Los_Angeles", 5, 0, 0).getTime(),
                new PlanSpec.MySchedule("America/Chicago",7,0,0).getTime());

        // Even the local time is the time, logically the two MySchedule instances are not the same because
        // they have different time zone name.
        Assert.assertNotEquals(
                new PlanSpec.MySchedule("America/Los_Angeles", 5, 0, 0),
                new PlanSpec.MySchedule("America/Chicago",7,0,0));

        // Make sure the backup time for AUTHOR, PROOF and TRUTH are sometime apart to do the docker stop/start and
        // copy the repo.

        final int minutesApart = PlanSpec.SCHEDULE_MINUTES_APART;

        Assert.assertEquals(PlanSpec.Branch.AUTHOR.getSchedule().plusMinutes(minutesApart).toString(),
                PlanSpec.Branch.TRUTH.getSchedule().toString());
        Assert.assertEquals(PlanSpec.Branch.TRUTH.getSchedule().plusMinutes(minutesApart).toString(),
                PlanSpec.Branch.PROOF.getSchedule().toString());

        Assert.assertEquals(new PlanSpec.MySchedule("UTC", 6, 0, 0).getTime(),
                new PlanSpec.MySchedule("America/Los_Angeles",22,0,0).getTime());

        for (PlanSpec.Branch b : PlanSpec.Branch.values()) {
            System.out.println(b.getSchedule().toString());
        }

        Assert.assertEquals(new PlanSpec.MySchedule("UTC", 10, 0, 0).getTime(),
                PlanSpec.Branch.AUTHOR.getSchedule().getTime());
    }
}